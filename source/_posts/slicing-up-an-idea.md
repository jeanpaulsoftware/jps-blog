---
title: Writing a Game in Ten Years or Less - Part 2
date: 2021-12-30 13:43:58
tags:
  - gamedev
  - unreal
---

So I've got a game concept and now we need to build it. How do you actually do that?

Well at this point I don't know *entirely*, and that's where all the fun is going to come from. It's also where all of the headaches are going to come from. But let's start with what I do know:

1. Game engines exist. Game engines mean you don't have to write all of the code, just the bits that make your game interesting.
2. Some of them are free (or mostly free).
3. Some of them have VR support.

Now I could write my own game engine but honestly fuck that, so let's pick someone else's. It's basically a choice between Unreal, Godot and Unity. You can do VR in all of them; they're all free (aside from profit share for Unreal and a license for Unity if you break $100k in sales). There's also CryEngine, but I still remember them saying that a "frozen jungle" was a great idea for Crysis so that one's out.

There's a lot written about how to pick between them, and nearly all of the conclusions are along the lines of "they're all great but you should consider which is right for your project", which doesn't fucking help anyone. What it really comes down to is this: **how scared are you of C++**

A friend of mine once described C++ as "the Latin of programming languages", which is to say it's got a weird set of rules and conventions that are hard to learn and that you're unlikely to have encountered unless you went to the right kind of school, but ultimately C++ is what most other languages spring from (functional programming languages excluded).

If you're scared of C++, pick Unity, because that's in C#. If you're not, pick Godot or Unreal.

C++ in it's newest form isn't nearly as hard work as it was 20 years ago, and I've just finished [The Jean-Paul Software Screen Explosion](https://store.steampowered.com/app/1631280/The_JeanPaul_Software_Screen_Explosion/) which was written in C++ with OpenFrameworks, so right now I'm OK with C++. On top of that I've not done anything non-trivial in C#, and life's too short to be learning new programming languages all the time. 

Now it's between Godot and Unreal, and I think I'm just going to pick Unreal. My reasoning is: I'm probably not going to make a lot of money out of this, but there's a lot more jobs out there for Unreal devs than there are for Godot devs, so it sets me up for the future a bit better, and if I want to pick up collaborators it'll be much easier to find one who's interested. 

Unreal also has excellent learning resources, it's far more mature, and the VR starter project puts you in a room with a gun... which is basically the game I'll be making, so I can immediately start thinking about the interesting bits.

![Chekhov's Gun](/images/posts/unreal-vr.jpg)

Now that's decided, what are the problems I need to solve?

1. Work out how to make the gun shoot properly fast bullets, as discussed in the previous post. Right now it shoots out balls that move incredibly slowly; what I need is a raycast weapon, which is to say one which projects out a line the instant you press the trigger and destroys any targets it touches.
2. Work out the mechanics of transitioning between lots of small levels in a seamless way.
3. Work out how game mechanics actually work in Unreal.

After I've worked these out I can start prototyping some levels. 

As it turns out, I was able to do number 1 in about five minutes:

![Blueprints in action](/images/posts/raycast-blueprint.png)

Unreal's Blueprint system is pretty amazing, and remarkably intuitive. I've not had to write a single line of code and already I'm a third of the way there. It's not actually destroying any targets yet, but that's not going to be a big ask- [there's a course that goes through doing exactly that](https://www.unrealengine.com/en-US/onlinelearning-courses/creating-a-first-person-aim-trainer), albeit not in VR.

Number 3 seems pretty trivial, but number 2 is going to be a bit harder, because it will require some experimentation. There's two options that I can see:

1. Use the new UE5 [World Partition](https://docs.unrealengine.com/5.0/en-US/WorldFeatures/WorldPartition/) system and teleport the player around after each game, having the engine take care of all the asset streaming. Will this still be quick if there are 200 minigames? I don't know.
2. Construct a separate level for each minigame. Will the levels load fast enough like this? I don't know.

So unfortunately I'm going to have to try both. The second option is simpler, but I don't know what the loading overhead is going to be like. The first option is more complex, but will it be a big drag loading loads of stuff when you first start the game? That's going to be the subject of the next post.

