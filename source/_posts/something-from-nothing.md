---
title: Writing a Game in Ten Years or Less - Part 1
date: 2021-12-28 13:36:05
tags: 
  - gamedev 
  - unreal
---

There's three main steps to writing a game:

1. Decide you want to write a game
2. Write the game
3. Profit

So, with step one out of the way, let's move onto step two, which has a few sub-tasks. 

First, what game is it I want to make? I've had a few ideas over the years.

### Einstein on the Rampage
Albert Einstein did not die in 1955; he simply became sick of public life. He moved to Winnepeg, Canada, and worked in his shed on a tachyon-powered anti-ageing chamber that could be used to extend his life. He tended his garden. He bred dogs. He started following the local folk and country circuit.

In the 1960s Albert became a big fan of a local musician by the name of Neil Young. Albert loved his work- Neil couldn't really sing or play, but if you listened past the shoddy musicianship the ideas were always great. 

Years pass.

In 1974 Neil Young releases 'On The Beach'. Einstein loves it. Inspired, he starts learning guitar and harmonica. A plan starts to form: he will make a big comeback to the world, by releasing a cover album of 'On The Beach', and doing the talk show circuit to market both his anti-ageing research and new record. In 1975 EMI has the rights to 'Einstein: On the Beach', and the release date is set for July 30th 1975. 

<iframe width="100%" height="400" src="https://www.youtube.com/embed/Cby46XsFm7Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

But then disaster strikes: five days before release, modernist composer Phillip Glass premiers his opera 'Einstein on the Beach'. 

It is a huge success. EMI pulls Albert's album; Glass is all over the press, and there is no way for Albert's release to get the publicity it requires. 

And so, Albert buys a shotgun, grabs his dark matter glove, opens a wormhole, and goes back in time to 1937 in order to kill Phillip Glass' parents.

### Chop
Chop is a helicopter simulator, except the helicopter blades are operated manually by the player whirling a motion controller above their head, and they have to steer by putting their keyboard on the floor and using the buttons with their feet.

### Haringay's Ladder
A psychological horror / management simulator / puzzle game set in North London where a landlord is forced to come to terms with the misery he's inflicting on his tenants, all while trying to increase his property empire. 

![The Haringey Ladder](/images/posts/Haringey-Passage-13.jpg)

Each turn has a 'planning' phase and a 'haunting' phase- the planning phase has you buy new properties, adjust rents and attempt to divide up existing ones into ever smaller flats (whilst maintaining "legal standards" for living spaces), the 'haunting' phase has you deal with the wretched bastard you have become.

## ANYWAY

All of these have problems- unknown complexity, potentially massive scope, etc- but the main one is I thought of the name first and then attempted to work backwards to a game idea. 

 What I should be doing is coming up with a game idea *first* and then worrying about the name later. So here's that idea: **Point Blank VR**

 If you're not familiar with Point Blank, it's a 90s arcade shooting game, made up of lots of smaller minigames. Here's some guy with a lot of spare space and income playing it:

 <iframe width="100%" height="400" src="https://www.youtube.com/embed/1YgL8yUsAIo?start=72" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This idea ticks a lot of boxes:

- *It's in VR*. I want to do something in VR, because I find it interesting.
- *It's pretty simple*. Shooting a gun in VR is just about the simplest thing you can program
- *It's got a lot of room for trying out different ideas*. Like with [The Jean-Paul Software Screen Explosion](https://store.steampowered.com/app/1631280/The_JeanPaul_Software_Screen_Explosion/) if I decide I need to learn something new, I can write a minigame that includes some element of it.
- *It's easy to work out whether it's going to be fun or not*. I don't have to write a deep character progression system or a plot or special balancing rules or anything like that- you have a gun, that's it. I can work up a prototype and test it relatively quickly, so I can fail things quickly if they're not entertaining.
- *It's easy to extend with other actions*- maybe you get a bow and arrow this level, maybe it's a two-handed rifle, maybe it's two guns, maybe it's a cricket bat, maybe you have to touch a particular bit of a horse... whatever. If I want to shake it up I can.
- *I don't have to worry too much about having a coherent art style*. In fact, it might be better if it's all over the place. 
- *It's an easy experience to share with other people in the same room*. Many of the best VR experiences are the ones you can take turns on- Beat Sabre, Ragnarock, Gorn. 

But it's also got some challenges

- *It's in VR*. I've not done anything in VR before.
- *Requires a lot more ideas*. I need to come up with enough game ideas and modules to keep things interesting. Five isn't enough, 20 might be, the latest WarioWare has 200 and that's a big ask.
- *Potentially awkward technical structure*. It needs to be responsive, and support rapid switching between games, and rules. It should be like WarioWare in that regard- you play something, one second later you're playing something else. No long loading screens, but also a variety of scenery.

Here's WarioWare if you've not played it- there's a new game every ten seconds. I don't think I need it that frantic, but every 60 seconds for 20 minutes is still 20 games.

<iframe width="100%" height="560" src="https://www.youtube.com/embed/Labv709P3Sk?start=596" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- *More games = more art* - I think I can get around this by adopting a collage-like style, but there's potentially a lot of art that needs to be created, and that could get challenging.

## Prior Art

So what's out there that's similar to this?

### Hotdogs, Horseshoes and Hand Grenades

This one is basically gun porn with a few shooting range modes thrown in, but the dev has given a great deal of thought into how the guns work in VR. It's more of a toybox than a game- you get to muck about with guns and grenades, without moving to the United States or the Ukraine. It's also got this whole thing about meat, which I don't quite understand. Is it some sort of GOP code signal? I think they're just trying to have a Sense Of Humour, but it comes across as *wacky*, which is a death sentence for comedy in my book.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/FqsuKKlo4ck?start=86" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**What it does really well:** gun handling, large variety of actions.
**What it does less well:** sterile environments, creepy obsession with sausages

### Space Pirate Trainer

It's a wave-based FPS shooter- drones fly about, you shoot them with a selection of weapons.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/ssasm7cYGmk?start=41" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

My main problem with this game is that the guns all fire slow-moving projectiles, which makes actually hitting anything a real pain in the arse. There's also not a lot of feedback when you *do* hit something, giving it that horrible swimming-through-treacle feeling that I really want to avoid. But, it's focussed, it does one thing very well, and it's very easy to understand what's going on.

**What it does really well:** very easy to understand what you're supposed to do: you shoot the things
**What it does less well:** plays like a game about swatting flies with a feather duster

The sound track is terrible though. This leads me on to:

### Pistol Whip

"Beat Sabre with Guns" is the pitch here, but really it's closer to Virtua Cop, the grandfather of arcade light gun games

It gets right pretty much everything Space Pirate Trainer gets wrong- shooting an enemy happens instantly, and most of the enemies you only have to hit once, so if you hit something you know it, and that always feels great.

My main issue with Pistol Whip is that a) it takes itself *incredibly seriously* and b) it's full of fucking dubstep.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/duCuMxHos-A?start=10" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*STOP PUTTING DUBSTEP AND GLITCHY BEATZZZ IN GAMES PLEASE*. I get it, Beat Sabre casts a long shadow, and that's packed with what the US media called EDM in 1998, but that doesn't mean *everyone has to do it*. Pistol Whip, Synth Riders, Audica, Until You Fall, Against, a game I just saw called Hard Bullet... it's not good, it's not interesting, and it frequently goes with that psuedo-80s pink-and-blue colour scheme over an image of a car driving into an extra-thick wireframe sunset that *wasn't ever cool, particularly not in the 1980s*. I was nine at the end of the 80s and even I know that.

Remember Outrun? The thing you're trying to hark back to? You know what music that had? It a mad mix of disco and latin jazz. It was synthesised, sure, because that's what the chip running the software could do. But in the composers' heads it didn't sound like sawtooth waves, it sounded like this:

<iframe width="100%" height="400" src="https://www.youtube.com/embed/Zw_AT5cNa3s?start=10" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So make your game sound like that! Look I can't criticise too much because I haven't written the game yet, but I can promise that the music isn't going to sound like Pistol Whip's.

**What it does really well:** good shooting, instant response, good feedback. It also has a degree of auto-aim applied- possibly something to consider.
**What it does less well:** fucking dubstep, takes itself too seriously, general stench of carb-watching tech-bro Displate-buying culturally-impoverished Redditcentric US suburbian crypto brony gun-buying dankmeme gamingchair paintball RGB mancave nerds trying to recapture their first memory of seeing Optimus Prime

### Nuclear Throne

This isn't a VR game but it is one of the best games about shooting things ever.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/JuXwOXG9VhQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The main thing that I want to take from Nuclear Throne is the level of skill that it will respond to, as opposed to the raw difficulty. It's a hard game- I'm not sure I want to make a hard game. But, I would like to make a game that has optional challenges, and responds well to those who manage to meet them. Nuclear Throne has a number of hidden bosses and areas, and getting to them all requires a lot of skill. But it's a great feeling when you get through. 

It's also got wonderful feedback when you shoot something- there's a noise and a flash, and it never feels like bullets have no impact.

**What it does really well:** amazing feedback visual and audio feedback, very tight gameplay
**What it does less well:** it's not in VR; for many this will be a benefit

## Pre-registering Game Design Goals

So with all that, I'd like to take a slightly rationalist approach and pre-register some goals that I can use to work out if I've made the game I want to make or if I've got lost on the way.

### 1. My Dad should be able to play it without me telling him what to do

Pick up and play, as far as is possible with VR. Any instruction should be obvious; very few instructions should be required

### 2. Getting it right should feel good

When you shoot something it should a) explode immediately, b) make a noise, and c) have some haptic feedback on the motion controller if availble

### 3. Getting it really right should feel even better

If you complete a level perfectly, you get a special prize. The special prize is possibly a harder challenge that makes you feel really smug for beating.

### 4. Getting it wrong shouldn't matter too much

There's prizes for getting things right, but you can still have fun if you can't shoot for toffee.

### 5. Easy to pass the headset around

You should be able to get this out with your mates round and have a good time. I've also got some ideasa about a party mode, but I need to get the basics in place.

### 6. Good weird, not bad weird

Playing Boggle with guns at a simulated motorway service station: good weird. Sausages everywhere: bad weird.

### 7. No fucking dubstep

The kids aren't into it. Nobody is into it. Anyone who likes Skrillex is in their 30s now, give it a rest.

## Step 3: Profit

In the next post I'm going to go into my thinking about how I'm actually going to build this, and how I'm going to go about finding out how to do the things I don't yet know to do.