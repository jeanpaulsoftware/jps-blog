---
title: Games (and Screensavers) of the Year 2021
date: 2021-12-21 12:04:49
tags: offtopic
---

These are my entries for The Gameological Society's yearly games of the year awards. The Gameological Society was once a subsection of The Onion AV Club, and was one of the most interesting and progressive areas in games writing while it was around. 

The 'What are you playing this weekend?' article format originated there; another favourite was their 'taste test' feature, where they would 'taste test' a game along with some user-submitted food item. 

I sent them some Marmite; they tried eating it with a spoon. 

Gameological evaporated after a few years of general brilliance, but the wonderful community has stuck around on Steam and Discord, and I occasionally stick my head in. 

Anyway, these are the categories:

1. Game of the Year 
2. Single-player GOTY
3. Multiplayer GOTY 
4. "Hindsight is 20/21" (a game you thought you liked only to discover you don't)
5. Favorite Replay ( / ongoing) 
6. Backlog GOTY Award (games from a previous release year) 
7. Didn't Click Award
8. Most Forgettable Award
9. Unexpected Joy 
10. Best Music 
11. Favorite Game Encounter 
12. Best DLC of the Year 
13. Most accessible game
14. "Waiting for Game-dot" ('I'll get around to it eventually' suggestion, for those games gathering dust) 
15. Game that made me think (from "slightly dubious essay" suggestion) 
16. Girlfriend Reviews Reward (your favorite game to watch playthroughs / streams of)
17. "Glad I stuck with it" Award
18. WILDCARD (could be art direction, plot, anything you want to recognize!)

And here are my picks

### Game of the Year: The Jean-Paul Software Screen Explosion
I am picking this because despite it not strictly speaking a game, it's the first non-web game-like thing I have written and released entirely by myself, and because writing it has been, almost literally, my game of the year. It's not perfect, but I've done it, and even sold a few copies, so that, for me, is good enough. Not good enough to retire on, but we'll sort that out with the next project.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/RO_lbUV3X7s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

There's a whole bunch of shit nobody tells you about when writing your own thing, and I am really quite proud that I managed to muster the bloody-mindedness to work through things like creating trailers and installers and driver issues and the Windows API. I'd not touched C++ since University either, so that was a big step up.

Have I mentioned that you can [buy it on Steam?](https://store.steampowered.com/app/1631280/The_JeanPaul_Software_Screen_Explosion/) Because you totally can.

### Single Player GOTY: Deathloop
### Multiplayer GOTY: Also Deathloop 
Arkane do it again; it's not quite as good as Prey, but it's given me some of my favourite gaming moments of the year. The single-player aspects of it are good enough to carry it- a plot to unravel, a world to explore- but the multiplayer is what elevates it to something really special. Like Dark Souls (almost exactly like Dark Souls) other players can invade your game and try and kill you. 

Given that you're trying to perform a certain sequence of actions in a certain order and do it perfectly in order to beat the game, when someone invades it means you have to drop all the plans you had and focus on the attacker. And the attacker can do all the things that you can, so you have to be ready for pretty much anything. They can go invisible and snipe you from a distance. They can make themselves near-invulnerable and run up to you with a shotgun. They can tie your fate to one of the NPC goons and shoot said goon in the face. They can sneak up behind you and stab you in the back. And if they manage to win, you have to start the day again.

![Show some respect rookie](/images/posts/0451.png)

And so my favouite gaming moment of the year: getting invaded on the final map whilst going for my first perfect loop. Fifteen minutes of very tense stealth and spotting each other on distant rooftops, firing off a shot, and trying to get a better position finally ended when I managed to sneak up behind them on the roof of a castle and kick them into the sea. Enemy defeated, I cleaned up the rest of the targets, and rolled through to the ending with a big grin on my face.

### Hindsight is 2021: Dyson Sphere Program
Some games have such complicated mechanics that they become almost entirely like programming a computer; Dyson Sphere Program is one of these games. The problem is, I do that for a living, and so if I'm going to do that for fun I'd rather work on my own projects. It's obviously brilliant, and I have absolutely no excuse to play it.

### Favourite Replay / Ongoing: Outer Wilds Echoes of the Eye
### Best DLC of the year: Outer Wilds Echoes of the Eye
The Outer Wilds is a masterpiece, and they've managed to add DLC that's just as good, AND that takes place in the same 22 minute time span as the original. It's been long enough since I completed the main game that going back contains enough mystery, but working out how to get to the DLC and then still being surprised by it has been a real treat. It'll be a while yet before I finish it, but finish it I will.

### Backlog Award: Apex Legends
The best game I absolutely suck at.

### Didn't Click Award: Loop Hero
Do you like waiting for things to happen? Do you like having minimal control over a chaotic system that gives minimal rewards and punishes you with the feeling of wasted time? Well Loop Hero is the game for you! Or at least, Loop Hero is not the game for me. It requires just enough attention that you can't leave it open on a separate screen, but not so much that anything really happens while you're looking at it. 

### Most Forgettable Award: Metroid Dread
I played it. I enjoyed it. I even got 100% of the items! And then it completely cleared itself from my mind.

### Unexpected Joy: Inscryption
The joy here is somewhat short lived: after completing the first 'act', the game reveals an entirely new section of itself that is similar but totally different to the first. Trouble is, after the initial elation, it turns out to be a bit of a miserable grind. The first part is sublime, but it never quite gets back to that level. 

A big part of the game is the surprises, and without acts two and three they wouldn't be able to cram as many in; but it would have been the better game if it had been a third of the length.

### Best Music: Death's Door
Don't take my word for it though, just listen to the damn thing:
<iframe src="https://open.spotify.com/embed/album/3P2Fkoqjg038YrPzDs0gFC?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

Now go and play the game.

### Favourite Game Encounter: Ragnarock and the leaderboard reset
Ragnarock is the only VR rhythm music game that isn't backed by 'synthwave' and / or fucking dubstep, and so is an immediate win in my book. You're on a Viking longboat, banging the drums to make your crew row at the right speed... to a selection of absurd metal songs. There's a few too many with bagpipes in, but there's also a collection by a band called Wind Rose who seem to be entirely about writing songs about dwarves who like mining things. Check this shit out:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/3uTysX2FsyQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Anyway there's this fucker by the name of Captain CLAWWW who is at the top of every single leaderboard for this game. There's also an achievement you can unlock for posting the number one score. My highest genuine score puts me in the top 50. As such: good luck getting it without being a much, much better drummer than I am- and I've got a Grade 8 drumkit certification.

Luckily when they came out of Early Access they reset the leaderboards, and I was READY: straight in, found a track nobody was going to be playing- a mid tier one half-way down the list with zero bragging rights attached- and went for it, and got it. And now I have this, and you never will:

![This is almost impossible](/images/posts/odin.png)

### Most Accessible Game: The Jean-Paul Software Screen Explosion
Have I mentioned that screensavers don't have complicated control schemes

### "Waiting for Game-dot": Pathologic 2
I've had this staring at me in my Steam library for ages. I really want to play it. For some reason I haven't. I loved the first game before I gave up on it; it's probably the feeling that exactly the same will happen here. I've also got too many New Years' resolution candidates already, so I'll just need to play it before it seems hopelessly dated.

### Game that Made Me Think: Halo Infinite
Halo Infinite has the absolute worst dialogue I've heard in some time. The baddies are all scenery-chewing pantomime villians; your AI companion seems to have been written *by* an AI, trained on a combination of rejected Joss Whedon scripts and casual conversation with a start-up social media manager. The other Named Character is voiced by the actor who does Octane in Apex Legends, and has the same accent, except they've made him a whiny fuck who just wants to get home to his family and probably has some redemption character arc that I will never see because I gave up after an hour and a half.

Anyway, your AI companion, in addition to sounding like she's doing one of those adverts you get on Instagram that are shot on a mobile phone and have an attractive young person talk straight to camera about this Amazing Thing they've just discovered, and that they've looked all over for but never quite found until now, is called...

The Weapon.

![THE WEAPON](/images/posts/Halo-Infinite-the-Weapon.jpg)

EVERYTHING in Halo has a name of the for 'The X'; The Covenant, The Flood, The Banished, The Forerunners. I stopped playing when I found out the next mission was going to be at a place called The Conservatory. It made me realise: this naming scheme is everywhere, and I think this is the game that made me think about how much I hate it. Off the top of my head: Back 4 Blood has you up against The Ridden. Fallout 4 has The Institute, The Commonwealth, The Minutemen. I know for a fact there's a lot more examples than that.

It's lazy, it's cheap, and it's infuriating- but it's particularly bad in Halo, a game set on a structure that's been ripped entirely from one of Iain M. Banks' amazing books. Iain M. Banks' work is constantly surprising; Halo never is. 

The new one has a nice grappling hook though, so that's something.

### Girlfriend Reviews Reward (your favorite game to watch playthroughs / streams of): Slay the Spire
I don't have the brain for Slay the Spire. I am amazed by watching people who do. Perhaps they only upload the streams where everything goes right?

### "Glad I stuck with it" Award: The Jean-Paul Software Screen Explosion

<iframe width="100%" height="315" src="https://www.youtube.com/embed/4iGGL8i1qQw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[If you get it in the Winter sale, it's 25% off!](https://store.steampowered.com/app/1631280/The_JeanPaul_Software_Screen_Explosion/)

### Wildcard award: Psychonauts 2
I genuinely loved this game, even if it didn't quite hit the highs of Deathloop. It's wholesome without being cloying; weird without being "wacky"; it's actually funny, and the fighing system doesn't entirely suck. 

So that's it. I haven't enabled comments on this website yet so please just complain into the ether.