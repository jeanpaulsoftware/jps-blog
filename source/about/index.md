---
title: About Jean-Paul Software
date: 1905-06-21 15:22:31
---
Jean-Paul Software is some guy with a computer making awesome shit, or at least stuff he considers awesome.

You should very definitely buy [The Jean-Paul Software Screen Explosion](https://store.steampowered.com/app/1631280/The_JeanPaul_Software_Screen_Explosion/) on Steam right now.

[My personal site is here](http://edboucher.com) and you may also be amused by [BREXFEST](http://brexfest.eu), particularly if you were alive in 2016. I've also got [some pretty photos to look at](https://edboucher.myportfolio.com/)

If you need to get in touch you can [email me here](mailto:e&#100;&#46;b&#111;ucher+jps&#64;gmail&#46;com)